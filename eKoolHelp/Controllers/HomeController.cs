﻿using eKoolHelp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eKoolHelp.Controllers
{
    public class HomeController : Controller
    {
        eKoolEntities db = new eKoolEntities();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult TunniPlaan(DateTime? date, int klass = 0)
        {
            date = TempData["date"] is DateTime d ? d.Date : date;
            klass = TempData["klass"] is int i ? i : klass;
            DateTime datev = date ?? DateTime.Today;
            datev = datev.AddDays(-((int)datev.DayOfWeek + 6) % 7); // see peaks tagama, et jutt esmaspäevast
            ViewBag.Päevad = Enumerable.Range(0, 7).Select(x => datev.AddDays(x)).ToList(); // selle nädali päevad
            ViewBag.Tunnid = Enumerable.Range(8, 10).ToList(); // tunnid 8:00 - 17:00
            var tunniplaan = db.Schedules
                //.Where(x => klass == 0 || x.StudentGroupId == klass)
                .AsEnumerable()
                .Where(x => x.Date != null)
                .Where(x => x.Date.Value.Date >= datev && x.Date.Value.Date < datev.AddDays(7))
                .ToLookup(x => x.Date.Value.Date.AddHours(x.Date.Value.Hour));
            
            ViewBag.Tunniplaan = tunniplaan;
            ViewBag.klass = new SelectList(db.StudentGroups, "Id", "NameEst", klass);
            ViewBag.date = datev;
            ViewBag.StudentGroup = db.StudentGroups.Find(klass);
            ViewBag.StudentGroupId = klass;

            ViewBag.TEacherId = new SelectList(db.People.Where(x => x.IsTeacher), "Id", "LastName");
            ViewBag.SubjectId = new SelectList(db.Subjects, "Id", "NameEst");

            return View();
        }

        public ActionResult AddSchedule(int klass, DateTime date)
        {
            ViewBag.StudentGroupId = new SelectList(db.StudentGroups, "Id", "NameEst", klass);
            ViewBag.SubjectId = new SelectList(db.Subjects, "Id", "NameEst");
            ViewBag.TeacherId = new SelectList(db.People.Where(p => p.IsTeacher), "Id", "LastName");
            return View(new Schedule
            {
                Date = date,
                StudentGroupId = klass
            }

                );
        }

        [HttpPost]
        public ActionResult AddSchedule(Schedule schedule)
        {
            if (ModelState.IsValid)
            {
                db.Schedules.Add(schedule);
                db.SaveChanges();
                TempData["date"] = schedule.Date;
                TempData["klass"] = schedule.StudentGroupId;
                return RedirectToAction("Tunniplaan");
            }
            ViewBag.StudentGroupId = new SelectList(db.StudentGroups, "Id", "NameEst", schedule.StudentGroupId);
            ViewBag.SubjectId = new SelectList(db.Subjects, "Id", "NameEst", schedule.SubjectId);
            ViewBag.TeacherId = new SelectList(db.People.Where(p => p.IsTeacher), "Id", "LastName", schedule.TeacherId);
            return View(schedule);
        }

        public ActionResult RemoveSchedule(int id)
        {
            Schedule s = db.Schedules.Find(id);
            if (s == null) return RedirectToAction("Tunniplaan");
            TempData["date"] = s.Date;
            db.Schedules.Remove(s);
            db.SaveChanges();
            return RedirectToAction("Tunniplaan");

        }
    }
}
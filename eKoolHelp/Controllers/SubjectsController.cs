﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using eKoolHelp.Models;

namespace eKoolHelp.Controllers
{
    public class SubjectsController : Controller
    {
        private eKoolEntities db = new eKoolEntities();

        // GET: Subjects
        public ActionResult Index()
        {
            ViewBag.Õpsid = db.People.ToList();
            return View(db.Subjects.ToList());
        }

        public ActionResult AddTeacher(int id, int personId)
        {
            Person p = db.People.Find(personId);
            Subject s = db.Subjects.Find(id);
            if(p != null && s != null)
            {
                try
                {
                    s.Teachers.Add(p);
                    db.SaveChanges();
                }
                catch (Exception)
                {
                }
            }

            return RedirectToAction("Details", new { id });
        }


        // GET: Subjects/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Subject subject = db.Subjects.Find(id);
            if (subject == null)
            {
                return HttpNotFound();
            }
            ViewBag.Õpsid = db.People.ToList();
            ViewBag.personId = new SelectList(db.People, "Id", "FirstName");
            return View(subject);
        }

        // GET: Subjects/Create
        public ActionResult Create()
        {
            ViewBag.opetajaId = new SelectList(db.People, "Id", "FirstName");
            return View();
        }

        // POST: Subjects/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,NameEng,NameEst")] Subject subject, int? opetajaId)
        {
            if (ModelState.IsValid)
            {
                db.Subjects.Add(subject);
                db.SaveChanges();

                if(opetajaId != null)
                {
                    Person ops = db.People.Find(opetajaId.Value);
                    if (ops != null)
                    {
                        subject.Teachers.Add(ops);
                        db.SaveChanges();
                    }
                }
                return RedirectToAction("Index");
            }
            ViewBag.opetajaId = new SelectList(db.People, "Id", "FirstName");
            return View(subject);
        }

        // GET: Subjects/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Subject subject = db.Subjects.Find(id);
            if (subject == null)
            {
                return HttpNotFound();
            }
            return View(subject);
        }

        // POST: Subjects/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,NameEng,NameEst")] Subject subject)
        {
            if (ModelState.IsValid)
            {
                db.Entry(subject).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(subject);
        }

        // GET: Subjects/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Subject subject = db.Subjects.Find(id);
            if (subject == null)
            {
                return HttpNotFound();
            }
            return View(subject);
        }

        // POST: Subjects/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Subject subject = db.Subjects.Find(id);
            db.Subjects.Remove(subject);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

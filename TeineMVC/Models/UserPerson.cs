﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeineMVC.Models
{
    public class UserPerson
    {
        public AspNetUser User { get; set; }
        public Person Person { get; set; }

        public string UserId => User?.Id ?? "";
        public int? PersonId => Person?.Id;

        public string FirstName
        {
            get => Person.FirstName == "" ? User.FirstName : Person.FirstName;
            set { Person.FirstName = value; User.FirstName = value; }
        }
        public string LastName
        {
            get => Person.LastName == "" ? User.LastName : Person.LastName;
            set { Person.LastName = value; User.LastName = value; }
        }

        // samalaadsed propertid kõigile väljadele 
    }
}
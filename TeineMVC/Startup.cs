﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TeineMVC.Startup))]
namespace TeineMVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

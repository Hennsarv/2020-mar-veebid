﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeineMVC
{
    public static class E
    {
        public static string Join(this IEnumerable<string> m, string s)
            => string.Join(s, m);
    }
}
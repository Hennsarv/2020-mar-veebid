﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeineMVC.Models;

namespace TeineMVC.Controllers
{
    public class UserPController : MyController
    {
        // GET: UserP
        public ActionResult Index()
        {
            var qUsers = from u in db.AspNetUsers
                         join p in db.People on u.Email equals p.EMail into pu
                         from su in pu.DefaultIfEmpty()
                         select new UserPerson { User = u, Person = su };

            var qPersons = from p in db.People
                           join u in db.AspNetUsers on p.EMail equals u.Email into pu
                           from su in pu.DefaultIfEmpty()
                           select new UserPerson { User = su, Person = p };
            return View(qPersons.Union(qUsers).ToList());
        }

        // GET: UserP/Details/5
        public ActionResult Details(string id, int? personId, string userId)
        {
            return 
                View(
                        id == null || id == ""
                        ? new UserPerson
                        {
                            User = db.AspNetUsers.Where(x => x.Email == id).SingleOrDefault(),
                            Person = db.People.Where(x => x.EMail == id).SingleOrDefault()
                        }
                        : new UserPerson
                        {
                            User = db.AspNetUsers.Find(userId),
                            Person = db.People.Find(personId)
                        }
                );
        }

        #region removed unimplemented
        //// GET: UserP/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        //// POST: UserP/Create
        //[HttpPost]
        //public ActionResult Create(FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add insert logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        //// GET: UserP/Edit/5
        //public ActionResult Edit(int id)
        //{
        //    return View();
        //}

        //// POST: UserP/Edit/5
        //[HttpPost]
        //public ActionResult Edit(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add update logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        //// GET: UserP/Delete/5
        //public ActionResult Delete(int id)
        //{
        //    return View();
        //}

        //// POST: UserP/Delete/5
        //[HttpPost]
        //public ActionResult Delete(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add delete logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //} 
        #endregion
    }
}

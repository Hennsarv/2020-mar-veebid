﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TeineMVC.Models;

namespace TeineMVC.Controllers
{
    public class FilesController : Controller
    {
        private NorthwindEntities db = new NorthwindEntities();

        // GET: Files
        public ActionResult Index()
        {
            return View(db.DataFiles.ToList());
        }

        // võimaldab mul "vastuseid" hoida meeles
       [OutputCache(VaryByParam = "id", Location = System.Web.UI.OutputCacheLocation.ServerAndClient, Duration = 3600)]
        public ActionResult Content(int? id)
        {
            DataFile f = db.DataFiles.Find(id ?? 0);
            if (f == null) return HttpNotFound();
            return File(f.Content, f.ContentType);
        }

        // GET: Files/Details/5
 
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

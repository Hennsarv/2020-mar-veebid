﻿using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeineMVC.Models;

namespace TeineMVC.Controllers
{
    public class MyController : Controller
    {
        dynamic _tempBag;
        protected dynamic TempBag => 
            _tempBag ?? (_tempBag = new Bag(TempData));

        dynamic _sessionBag;
        protected dynamic SessionBag =>
            _sessionBag ?? (_sessionBag = new Bag(Session));

        dynamic _paramsBag;
        protected dynamic ParamsBag =>
            _paramsBag ?? (_paramsBag = new Bag(Request.Params));

        dynamic _applicationBag;
        protected dynamic ApplicationBag =>
            _applicationBag ?? (_applicationBag = new Bag(HttpContext.Application));

        HttpCookie bagCookie;
        protected HttpCookie BagCookie =>
            bagCookie
                ?? (bagCookie = Request.Cookies["bagtest"])
                ?? (bagCookie = new HttpCookie("bagtest"))
            ;
        dynamic cookieBag;
        protected dynamic CookieBag => cookieBag ?? (cookieBag = new Bag(BagCookie));
        

        protected void SetCookie(string name, object value, int hours = 1)
        {
            if (value == null) return;
            BagCookie.Values[name] = value.ToString();
            BagCookie.Expires = DateTime.Now.AddHours(hours);
            Response.Cookies.Add(BagCookie);
        }


        protected NorthwindEntities db = new NorthwindEntities();
        AspNetUser currentUser = null;
        protected AspNetUser CurrentUser =>
            currentUser ?? 
            (currentUser = db.AspNetUsers.Where(x => x.Email == User.Identity.Name).SingleOrDefault());

        Person currentPerson = null;
        protected Person CurrentPerson =>
            currentPerson ??
            (currentPerson = db.People.Where(x => x.EMail == User.Identity.Name).SingleOrDefault());
        protected void SaveDataFile(HttpPostedFileBase file, Action<int> action, int? old)
        {
            
            if (file != null)
            {
                using (BinaryReader br = new BinaryReader(file.InputStream))
                {
                    byte[] buff = br.ReadBytes(file.ContentLength);
                    DataFile f = new DataFile
                    {
                        Content = buff,
                        ContentType = file.ContentType,
                        FileName = Path.GetFileName( file.FileName),
                        Created = DateTime.Now
                    };
                    db.DataFiles.Add(f);
                    db.SaveChanges();

                    //Faili salvestamise ja vana faili kustutamise vahel on vaja teha
                    action(f.Id);

                    db.SaveChanges();

                    if (old != null)
                    {
                        DataFile df = db.DataFiles.Find(old);
                        db.DataFiles.Remove(df);
                        db.SaveChanges();
                    }
                }

            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TeineMVC.Models;

namespace TeineMVC.Controllers
{
    public class PostsController : MyController
    {

        // GET: Posts
        public ActionResult Index()
        {
            var posts = db.Posts.Include(p => p.Owner).Include("Likes");
            return View(posts.ToList());
        }

        // GET: Posts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        
        public ActionResult AddLike(int? id)
        {
            if (!Request.IsAuthenticated) return RedirectToAction("Index", "Home");
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }


            try
            {
                post.Likes.Add(new Like { Liker = CurrentUser });
                db.SaveChanges();
            }
            catch {}

            return RedirectToAction("Index");
        }

        [Authorize]
        // GET: Posts/Create
        public ActionResult Create()
        {
            //    var currentUser = db.AspNetUsers.Where(x => x.Email == User.Identity.Name).Single();
            //    return View(new Post { OwnerId = currentUser.Id });
            //
            ViewBag.OwnerId = new SelectList(db.AspNetUsers, "Id", "DisplayName");
            if (User.IsInRole("Administrator"))
                return View("AdminCreate", null);
            else
                return View();
        }

        // POST: Posts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,OwnerId,Subject,Content,PictureId")] Post post)
        {
            if (ModelState.IsValid)
            {
                //post.OwnerId = currentUser.Id; // käsitsi sidumine
                //db.Posts.Add(post);
                if (post.OwnerId == null)
                    CurrentUser.Posts.Add(post);  // automaatnhe sidumine
                else
                    db.Posts.Add(post);

                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.OwnerId = new SelectList(db.AspNetUsers, "Id", "DisplayName");
            return View(post);
        }

        // GET: Posts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        // POST: Posts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Subject,Content,PictureId")] Post post)
        {
            if (ModelState.IsValid)
            {
                db.Entry(post).State = EntityState.Modified;
                db.Entry(post).Property("OwnerId").IsModified = false;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(post);
        }

        // GET: Posts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        // POST: Posts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Post post = db.Posts.Find(id);
            db.Posts.Remove(post);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TeineMVC.Models;
using System.ComponentModel.DataAnnotations;

namespace TeineMVC.Models
{
    [MetadataType(typeof(EmployeeMetadata))]
    partial class Employee
    {
        public string FullName => $"{FirstName} {LastName}";

        [Display(Name = "Manager")]
        public string ManagerName => Manager?.FullName;
    }

    public class EmployeeMetadata
    {
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime? BirthDate { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime? HireDate { get; set; }
    }
}


namespace TeineMVC.Controllers
{
    public class EmployeesController : Controller
    {
        static Dictionary<string, string> Tiitlid = new Dictionary<string, string>
        {
            {"", "Inimene, kelle seisust me ei tea" },
            {"Dr.", "Inimene kellel on dohtrikraad - Dr." },
            {"Mr.", "Meesterahvas - Mr." },
            {"Miss", "Vaba naine - Miss" },
            {"Ms.", "Naine, kes ei ütle, kui vaba ta on - Ms." },
            {"Mrs.", "Inimene kellel on Mees - Mrs." },
            {"Jun.", "Inimene kes on alles noor - Jun." },

        };

        private NorthwindEntities db = new NorthwindEntities();

        // GET: Employees
        public ActionResult Index()
        {
            var employees = db.Employees;
            return View(employees.ToList());
        }

        // GET: Employees/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }

            

            return View(employee);
        }

        // GET: Employees/Create
        public ActionResult Create()
        {
            ViewBag.ReportsTo = new SelectList(db.Employees, "EmployeeID", "FullName");
            ViewBag.TitleOfCourtesy = new SelectList(Tiitlid, "Key", "Value");

            return View();
        }

        // POST: Employees/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "EmployeeID,LastName,FirstName,Title,TitleOfCourtesy,BirthDate,HireDate,Address,City,Region,PostalCode,Country,HomePhone,Extension,Photo,Notes,ReportsTo,PhotoPath")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                db.Employees.Add(employee);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ReportsTo = new SelectList(db.Employees, "EmployeeID", "FullName", employee.ReportsTo);
            ViewBag.TitleOfCourtesy = new SelectList(Tiitlid, "Key", "Value", employee.TitleOfCourtesy);

            return View(employee);
        }

        // GET: Employees/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            ViewBag.ReportsTo = new SelectList(db.Employees, "EmployeeID", "FullName", employee.ReportsTo);
            ViewBag.TitleOfCourtesy = new SelectList(Tiitlid, "Key", "Value", employee.TitleOfCourtesy);
            return View(employee);
        }

        // POST: Employees/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "EmployeeID,LastName,FirstName,Title,TitleOfCourtesy,BirthDate,HireDate,Address,City,Region,PostalCode,Country,HomePhone,Extension,Photo,Notes,ReportsTo,PhotoPath")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                db.Entry(employee).State = EntityState.Modified;
                db.Entry(employee).Property("Photo").IsModified = false;
                db.Entry(employee).Property("Notes").IsModified = false;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ReportsTo = new SelectList(db.Employees, "EmployeeID", "FullName", employee.ReportsTo);
            ViewBag.TitleOfCourtesy = new SelectList(Tiitlid, "Key", "Value", employee.TitleOfCourtesy);

            return View(employee);
        }

        // GET: Employees/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // POST: Employees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Employee employee = db.Employees.Find(id);
            db.Employees.Remove(employee);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

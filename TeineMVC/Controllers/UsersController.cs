﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TeineMVC.Models;

namespace TeineMVC.Controllers
{
    [Authorize(Roles="Administrator")]
    public class UsersController : Controller
    {
        private NorthwindEntities db = new NorthwindEntities();

        // GET: Users
        public ActionResult Index()
        {
            var aspNetUsers = db.AspNetUsers.Include(a => a.DataFile);
            ViewBag.Roles = db.AspNetRoles.Where(x => x.Name != "Administrator").ToList();
            ViewBag.Tagasi = "Index";
            return View(aspNetUsers.ToList());
        }

 
        // GET: Users/Details/5
        public ActionResult Details(string id)
        {
            //if (User.IsInRole("Administrator")) // programmiline rollikontroll

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AspNetUser aspNetUser = db.AspNetUsers.Find(id);
            if (aspNetUser == null)
            {
                return HttpNotFound();
            }
            ViewBag.Tagasi = "Details";
            ViewBag.Roles = db.AspNetRoles.ToList();
            return View(aspNetUser);
        }

       public ActionResult AddRole(string id, string roleid, string tagasi)
        {
            AspNetUser u = db.AspNetUsers.Find(id);
            AspNetRole r = db.AspNetRoles.Find(roleid);
            if(u != null && r != null)
            {
                try
                {
                    u.AspNetRoles.Add(r);
                    db.SaveChanges();
                }
                catch (Exception)
                {

                }
            }

            return RedirectToAction(tagasi, new { id });
        }
        public ActionResult RemoveRole(string id, string roleid, string tagasi)
        {
            AspNetUser u = db.AspNetUsers.Find(id);
            AspNetRole r = db.AspNetRoles.Find(roleid);
            if (u != null && r != null)
            {
                try
                {
                    u.AspNetRoles.Remove(r);
                    db.SaveChanges();
                }
                catch (Exception)
                {

                }
            }

            return RedirectToAction(tagasi, new { id });
        }

        // GET: Users/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AspNetUser aspNetUser = db.AspNetUsers.Find(id);
            if (aspNetUser == null)
            {
                return HttpNotFound();
            }
            return View(aspNetUser);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            AspNetUser aspNetUser = db.AspNetUsers.Find(id);
            db.AspNetUsers.Remove(aspNetUser);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

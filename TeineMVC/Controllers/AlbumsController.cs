﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TeineMVC.Models;

namespace TeineMVC.Controllers
{
    

    public class AlbumsController : MyController
    {
        //private NorthwindEntities db = new NorthwindEntities();

        // GET: Albums
        public ActionResult Index()
        {
            var albums = db.Albums; //.Include(a => a.DataFile).Include(a => a.AspNetUser);
            return View(albums.ToList());

        }

        // GET: Albums/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Album album = db.Albums.Find(id);
            if (album == null)
            {
                return HttpNotFound();
            }
            return View(album);
        }

        // GET: Albums/Create
        [Authorize]
        public ActionResult Create()
        {
            //ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "ContentType");
            //ViewBag.Owner = new SelectList(db.AspNetUsers, "Id", "DisplayName");
            return View();
        }
        [Authorize]
        // POST: Albums/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name")] Album album, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                var aspUser = db.AspNetUsers.Where(x => x.Email == User.Identity.Name).SingleOrDefault();
                // see on hetkel sisselogitud kasutaja
                album.Owner = aspUser.Id;
                db.Albums.Add(album);
                db.SaveChanges();

                //if(file != null)
                //{
                //    using (System.IO.BinaryReader br = new System.IO.BinaryReader(file.InputStream))
                //    {
                //        byte[] buff = br.ReadBytes(file.ContentLength);
                //        DataFile f = new DataFile
                //        {
                //            Content = buff,
                //            ContentType = file.ContentType,
                //            FileName = file.FileName.Split('\\').Last().Split('/').Last(),
                //            Created = DateTime.Now
                //        };
                //        db.DataFiles.Add(f);
                //        db.SaveChanges();

                //        album.PictureId = f.Id;
                //        db.SaveChanges();

                //    }

                //}

                SaveDataFile(file, x => album.PictureId = x, null);

                return RedirectToAction("Index");
            }

            //ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "ContentType", album.PictureId);
            //ViewBag.Owner = new SelectList(db.AspNetUsers, "Id", "DisplayName", album.Owner);
            return View(album);
        }

        // GET: Albums/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Album album = db.Albums.Find(id);
            if (album == null)
            {
                return HttpNotFound();
            }
             return View(album);
        }

        // POST: Albums/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,PictureId")] Album album, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                db.Entry(album).State = EntityState.Modified;
                db.Entry(album).Property("PictureId").IsModified = false;
                db.Entry(album).Property("Owner").IsModified = false;
                db.SaveChanges();
                
                SaveDataFile(file, x => album.PictureId = x, album.PictureId);

                return RedirectToAction("Index");
            }
             return View(album);
        }

        // GET: Albums/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Album album = db.Albums.Find(id);
            if (album == null)
            {
                return HttpNotFound();
            }
            return View(album);
        }

        // POST: Albums/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Album album = db.Albums.Find(id);
            db.Albums.Remove(album);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}

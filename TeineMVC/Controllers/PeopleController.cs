﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TeineMVC.Models;

namespace TeineMVC.Models
{
    partial class Person
    {
        
    }
}

namespace TeineMVC.Controllers
{
    public class PeopleController : MyController
    {
        //private NorthwindEntities db = new NorthwindEntities();

        // GET: People
        public ActionResult Index()
        {
            var people = db.People.Include(p => p.DataFile);
            ViewBag.Ootel = db.AspNetUsers
                .Where(x => !people.Select(p => p.EMail).Contains(x.Email)).ToList();

            return View(people.ToList());
        }

        public ActionResult AddPerson(string id)
        {
            AspNetUser u = db.AspNetUsers.Find(id);
            if (u != null)
            {
                if (u.BirthDate == null)
                {
                    TempBag.ErrorMessage = "inimesel peab olema sünnipäev, muidu me ei lisa";
                }
                else
                {
                    Person p = new Person
                    {
                        FirstName = u.FirstName,
                        LastName = u.LastName,
                        EMail = u.Email,
                        BirthDate = u.BirthDate.Value
                    };

                    try
                    {
                        db.People.Add(p);
                        db.SaveChanges();

                    }
                    catch (Exception e)
                    {
                        TempData["ErrorMessage"] = e.Message;
                    }
                }
            }
            return RedirectToAction("Index");
        }


        public ActionResult DenyPerson(string id)
        {
            AspNetUser u = db.AspNetUsers.Find(id);
            if (u != null)
            {
                try
                {
                    foreach (var l in u.Likes.ToList())
                        db.Likes.Remove(l);
                    db.SaveChanges();
                    
                    db.AspNetUsers.Remove(u);
                    db.SaveChanges();
                }
                catch (Exception e)
                {

                    TempBag.ErrorMessage = e.Message;
                }
            }
            return RedirectToAction("Index");

        }

        // GET: People/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // GET: People/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: People/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,FirstName,LastName,EMail,BirthDate,Nationality,PictureId")] Person person, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                db.People.Add(person);
                db.SaveChanges();
                SaveDataFile(file, x => person.PictureId = x, null);
                return RedirectToAction("Index");
            }

            return View(person);
        }

        // GET: People/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // POST: People/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,FirstName,LastName,EMail,BirthDate,Nationality,PictureId")] Person person, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                db.Entry(person).State = EntityState.Modified;
                db.Entry(person).Property("PictureId").IsModified = false;
                db.SaveChanges();
                SaveDataFile(file, x => person.PictureId = x, person.PictureId);
                return RedirectToAction("Index");
            }
            return View(person);
        }

        // GET: People/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // POST: People/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Person person = db.People.Find(id);
            db.People.Remove(person);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeineMVC.Models;

namespace TeineMVC.Controllers
{
    public class HomeController : MyController
    {
        //NorthwindEntities db = new NorthwindEntities();
        // funktsiooni, mis annab mulle emaili järgi AspNetUser objekti
        public static AspNetUser GetByEmail(string email)
        {
            using(NorthwindEntities db = new NorthwindEntities())
            {
                return db.AspNetUsers.Where(x => x.Email == email).SingleOrDefault();
            }
        }


        public ActionResult Index()
        {

            ViewData["tervitus"] = "Hello World!";

            ViewBag.Teretaja = ParamsBag.Nimi;

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            if(Request.IsAuthenticated)
            {
                var aspUser = db.AspNetUsers.Where(x => x.Email == User.Identity.Name).Single();
                var albums = aspUser.Albums;
                ViewBag.Albums = albums;
            }

            return View();
        }

        [Authorize] // COntact meetod on AINULT sisselogitud kasutajatele
        public ActionResult Contact()
        {
            ViewBag.Message = "Meie test lehekülg";

            TempBag.Esimene = "Esimene";
            TempData["Teine"] = "Teine";
            SessionBag.Kolmas = "Kolmas";
            Session["Neljas"] = "Neljas";

            SetCookie("Nimi", ParamsBag.Nimi, 1);
            ViewBag.Nimi = CookieBag.Nimi;

            ViewBag.AppRequests = ++ApplicationBag.Requests;
            ViewBag.SesRequests = ++SessionBag.Requests;
            ViewBag.SesNr = SessionBag.Nr;
            ViewBag.AppStarted = ApplicationBag.Started;
            ViewBag.SesStarted = SessionBag.Started;

            return View();
        }
        [HttpGet]
        public ActionResult Demo()
        {
            int i = 0;
            ViewBag.Variandid =
                new string[] { "IlusHenn", "NoorHenn", "KaabugaHenn" }
                .Select(x => new ValikStruct { Id = ++i, Valik = x })
                .ToList();
            return View();
        }

        [HttpPost]
        public ActionResult Demo(VastuseForm vorm, FormCollection coll)
        {
            
            ViewBag.Vastus =
                Enumerable.Range(1, 8)
                .Select(x => new { x, b = coll[x.ToString()] })
                .Select(x => $"{x.x}-{x.b}")
                .ToList();
            ViewBag.Vastus.Add("vajutati nuppu: " + vorm.nupp);
            ViewBag.Vastus.Add("Valiti valikust: " + vorm.valik);
            ViewBag.Vastus.Add("Peidetud väli: " + vorm.id);

            int i = 0;
            ViewBag.Variandid =
                new string[] { "IlusHenn", "NoorHenn", "KaabugaHenn" }
                .Select(x => new ValikStruct { Id = ++i, Valik = x })
                .ToList();
            return View();

        }
    }
}

namespace TeineMVC.Models
{
    public class ValikStruct
    {
        public int Id { get; set; }
        public string Valik { get; set; }
    }

    public class VastuseForm
    {
        public string nupp { get; set; }
        public string valik { get; set; }

        public int id { get; set; }
    }
}
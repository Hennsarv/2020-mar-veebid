﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MisOnBag
{
    class Program
    {
        static void Main(string[] args)
        {
            // enne teen mõne näidise dynamikutega
            int ii = 7; // alati int
            object io = 7; //
            //io = "seitse"; // uus objekt

            ii++;
            io =  (int)io + 1;  // io++ ei saa teha

            dynamic id = 7;   // late bounded
            id++;
            Console.WriteLine(id);
            id = "seitse";
            Console.WriteLine(id);

            dynamic henn = new MyBag();
            henn.Nimi = "Henn Sarv";
            henn.Vanus = 64;
            henn.Vanus++;
            Console.WriteLine(henn.Vanus);

            Dictionary<string, dynamic> asjad = new Dictionary<string, dynamic>
            {
                { "asi1" , "Sinine" },
                { "muuasi", 77 }
            };
            Console.WriteLine(asjad["muuasi"]);

            dynamic asjaBag = new MyBag(asjad);
            Console.WriteLine(asjaBag.asi7);

        }
    }

    class MyBag : DynamicObject
    {
        dynamic bag;

        public MyBag() => bag = new Dictionary<string, dynamic>();
        public MyBag(IDictionary<string,dynamic> dict) => bag = dict;

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            result = bag.ContainsKey(binder.Name) ? bag[binder.Name] : null;
            return true;
        }

        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            bag[binder.Name] = value;
            return true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EsimeneMVC.Controllers
{
    public class HomeController : Controller
    {
        public string Tere(string id) => $"Tere {id}!!!";

        public ActionResult Sipsik()
        {

            ViewBag.Sõbrad = new string[] { "Henn", "Ants", "Peeter" };

            return View();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult TestRadio(string vastus1, string vastus2)
        {
            ViewBag.Vastused = $"vastus1={vastus1}&vastus2={vastus2}";
            ViewBag.Kysimus1 = new string[] { "Lammas", "Lehm", "Hobune" };
            ViewBag.Kysimus2 = new string[] { "Auto", "Mootorratas", "Jalgratas", "Hobune" };

            return View();

        }
    }
}
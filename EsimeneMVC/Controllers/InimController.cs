﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EsimeneMVC.Models;

namespace EsimeneMVC.Controllers
{
    public class InimController : ApiController
    {
        // GET: api/Inim
        public IEnumerable<Inimene> Get()
        {
            return Inimene.inimesed.Values;
        }

        // GET: api/Inim/5
        public Inimene Get(int id)
        {
            return Inimene.inimesed.ContainsKey(id)
                ? Inimene.inimesed[id] : null
                ;
        }

        // POST: api/Inim
        public Inimene Post([FromBody]Inimene value)
        {
            value.Nr = Inimene.inimesed.Values.Max(x => x.Nr) + 1;
            Inimene.inimesed.Add(value.Nr, value);
            return value;
        }

        // PUT: api/Inim/5
        public void Put(int id, [FromBody]Inimene value)
        {
            if(value.Nr == id)
            {
                Inimene.inimesed[id] = value;
            }
        }

        // DELETE: api/Inim/5
        public void Delete(int id)
        {
            Inimene.inimesed.Remove(id);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EsimeneMVC.Models;

namespace EsimeneMVC.Controllers
{
    public class UsersController : Controller
    {
        private NorthwindEntities db = new NorthwindEntities();

        // GET: Users
        public ActionResult Index()
        {
            ViewBag.Roles = db.AspNetRoles.ToList();
            return View(db.AspNetUsers.Include("AspNetRoles").ToList());
        }

        public ActionResult AddRole(string id, string roleId)
        {
            AspNetUser u = db.AspNetUsers.Find(id);
            AspNetRole r = db.AspNetRoles.Find(roleId);
            if (u != null && r != null)
            {
                try
                {
                    u.AspNetRoles.Add(r);
                    db.SaveChanges();
                }
                catch { }
            }
            return RedirectToAction("Index");
        }

        public ActionResult RemoveRole(string id, string roleId)
        {
            AspNetUser u = db.AspNetUsers.Find(id);
            AspNetRole r = db.AspNetRoles.Find(roleId);
            if (u != null && r != null)
            {
                try
                {
                    u.AspNetRoles.Remove(r);
                    db.SaveChanges();
                }
                catch { }
            }
            return RedirectToAction("Index");
        }


        // GET: Users/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AspNetUser aspNetUser = db.AspNetUsers.Find(id);
            if (aspNetUser == null)
            {
                return HttpNotFound();
            }
            return View(aspNetUser);
        }

        public ActionResult CreateContact(string id)
        {
            Contact contact = new Contact { UserId = id };
            return View(contact);
        }

        [HttpPost]
        public ActionResult CreateContact(Contact contact)
        {
            db.Contacts.Add(contact);
            db.SaveChanges();
            return RedirectToAction("Details", new { id = contact.UserId });
        }

        public ActionResult RemoveContact(int id)
        {
            Contact c = db.Contacts.Find(id);
            string userId = c.UserId;
            db.Contacts.Remove(c);
            db.SaveChanges();
            return RedirectToAction("Details", new { id = userId });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

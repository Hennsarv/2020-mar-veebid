﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EsimeneMVC.Startup))]
namespace EsimeneMVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations; // selle ma lisasin, et saaks all [Key] ütelda

namespace EsimeneMVC.Models
{
    public class Inimene
    {
        public static Dictionary<int, Inimene> inimesed = new Dictionary<int, Inimene>();

        static Inimene()
        {
            Inimene henn = new Inimene
            {
                Nimi = "Henn",
                Vanus = 65,
                Nr = 1
            };

            inimesed.Add(henn.Nr, henn);
        }

        [Key]public int Nr { get; set; }
        public string Nimi { get; set; }
        public int Vanus { get; set; }
    }
}
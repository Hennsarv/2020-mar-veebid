//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PuhkuseHelp.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Attachment
    {
        public int Id { get; set; }
        public int VacationId { get; set; }
        public int FileId { get; set; }
        public string Comment { get; set; }
    
        public virtual DataFile DataFile { get; set; }
        public virtual Vacation Vacation { get; set; }
    }
}

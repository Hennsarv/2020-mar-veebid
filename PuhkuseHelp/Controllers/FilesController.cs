﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PuhkuseHelp.Models;

namespace PuhkuseHelp.Controllers
{
    public class FilesController : Controller
    {
        VacationEntities db = new VacationEntities();

        public ActionResult Content(int id = 0)
        {
            DataFile f = db.DataFiles.Find(id);
            if (f == null) return HttpNotFound();
            return File(f.Content, f.ContentType);
        }
    }
}
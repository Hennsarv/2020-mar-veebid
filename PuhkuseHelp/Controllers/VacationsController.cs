﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PuhkuseHelp.Models;

namespace PuhkuseHelp.Models
{
    partial class Vacation
    {
        public int Vacations => Person.Vacations.Where(x => x.Id != Id).Count();

        public int? UsableDays => 
           
            (
            (Person.StandardDays + Person.ExtraDays??0) -
            Person.Vacations
                .Where(x => x.Id != Id)
                .Where(x => x.StartDate != null && x.EndDate != null)
                .Where(x => StartDate.Value.Year == DateTime.Now.Year)
                .Where(x => x.StartDate < StartDate)
                .Sum(x => (x.EndDate.Value - x.StartDate.Value).Days)
                )
            ;
    }
}

namespace PuhkuseHelp.Controllers
{
    public class VacationsController : Controller
    {
        private VacationEntities db = new VacationEntities();

        // GET: Vacations
        public ActionResult Index()
        {
            var vacations = db.Vacations.Include(v => v.Category).Include(v => v.Person);
            return View(vacations.ToList());
        }

        // GET: Vacations/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vacation vacation = db.Vacations.Find(id);
            if (vacation == null)
            {
                return HttpNotFound();
            }
            return View(vacation);
        }

        public ActionResult AddAttachment(int id, string comment, int vacationId, HttpPostedFileBase file)
        {
            // enne kontrolli, kas id == vacationId
            // enne veendu, et Vacation selle id-ga on olemas
            
            if (file != null)
            { 
                using(BinaryReader br = new BinaryReader(file.InputStream))
                {
                    try
                    {
                        db.Database.BeginTransaction(); // päris elus võiks siin olla selline "ümbris"
                        DataFile f = new DataFile
                        {
                            FileName = Path.GetFileName(file.FileName),
                            ContentType = file.ContentType,
                            Content = br.ReadBytes(file.ContentLength)
                        };
                        db.DataFiles.Add(f);
                        db.SaveChanges(); // nüüd saab f omale id

                        db.Attachments.Add(
                            new Attachment
                            {
                                VacationId = vacationId,
                                Comment = comment,
                                FileId = f.Id
                            }
                            );
                        db.SaveChanges();
                        db.Database.CurrentTransaction.Commit(); // ümbrise lõpp
                    }
                    catch (Exception)
                    {
                    }
                }
            }
            return RedirectToAction("Details", new { id });
        }

        // GET: Vacations/Create
        public ActionResult Create()
        {
            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Name");
            ViewBag.PersonId = new SelectList(db.People, "Id", "FirstName");
            return View();
        }

        // POST: Vacations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,PersonId,StartDate,EndDate,Duration,Notes,State,CategoryId")] Vacation vacation)
        {
            if (ModelState.IsValid)
            {
                db.Vacations.Add(vacation);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Name", vacation.CategoryId);
            ViewBag.PersonId = new SelectList(db.People, "Id", "FirstName", vacation.PersonId);
            return View(vacation);
        }

        // GET: Vacations/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vacation vacation = db.Vacations.Find(id);
            if (vacation == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Name", vacation.CategoryId);
            ViewBag.PersonId = new SelectList(db.People, "Id", "FirstName", vacation.PersonId);
            return View(vacation);
        }

        // POST: Vacations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,PersonId,StartDate,EndDate,Duration,Notes,State,CategoryId")] Vacation vacation)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vacation).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Name", vacation.CategoryId);
            ViewBag.PersonId = new SelectList(db.People, "Id", "FirstName", vacation.PersonId);
            return View(vacation);
        }

        // GET: Vacations/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vacation vacation = db.Vacations.Find(id);
            if (vacation == null)
            {
                return HttpNotFound();
            }
            return View(vacation);
        }

        // POST: Vacations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Vacation vacation = db.Vacations.Find(id);
            db.Vacations.Remove(vacation);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

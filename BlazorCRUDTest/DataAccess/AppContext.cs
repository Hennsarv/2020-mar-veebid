﻿using Microsoft.EntityFrameworkCore;

namespace BlazorCRUDTest.DataAccess
{
    public class AppContext : DbContext
    {
        public AppContext() { }

        public AppContext(DbContextOptions<AppContext> options) : base(options) { }
    }
}
